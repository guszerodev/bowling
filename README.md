# bowling

## Requirements

Create a Web API app using .Net 6 to record the score of a bowling game.

Requirements

- [x] API service to create a new game for one player.
- [x] API service to record the number of pins that were knocked down e.g “1”, “2”, “9”, “/”, “X”.
- [x] API service to get the Score.
- [/] Implement proper validation and error handling.
- [/] Ensure the API follows RESTful principles.
- [/] Implement unit tests to cover critical parts of the codebase.


No-Functional requirements

- [x] Use a N-tier architecture
- [x] Use of DI - IoC
- [x] Cache the get score service response if this is requested multiple times in 5 seconds.
- [x] Use db storage (SQL).
- [/] Optimize the API for high performance.

Optional

- [x] Add a second db storage (NoSQL).
- [x] Switch Db storage from the IoC.

Concepts to evaluate

Clean architecture
Clean Code
Code organization and structure.
Design patterns
Quality of unit testing - mock
SOLID principles

## Problem Analysis

The game consists of 10 frames. Normally, a player has two rolls per frame. However, if the first roll of a frame results in a strike (all pins are knocked down), the second roll of that frame is skipped, and the frame only has one roll. If all pins are knocked down on the second roll, it is called a spare. The tenth frame is special and can have up to 3 rolls if there is a strike or a spare, allowing for a maximum of 3 strikes.

### Points and Final Score Calculation

The default scoring method, used when there are no strikes or spares, is to sum the pins (points) for all ten frames. When a frame has a strike, it can earn bonus points from the next frames, specifically the next two rolls (Strike). A strike is worth 10 points, plus the bonus from the next two rolls. If the next frame does not have a strike or spare, the bonus points are equal to the sum of the rolls' points in that frame. If the next frame is also a strike, the bonus calculation continues by considering the next frame's roll points. But, if the next frame has a spare instead of a strike, the bonus is only 10 points, representing the score from that frame's two rolls. The tenth frame is a special case where there are no bonus points, but an extra roll is allowed if there are strikes or a spare (worth 10 points). To calculate the final game score, the scores of each frame are summed together.

### Edge Cases

Special attention should be given to calculating the scores for frames with strikes or spares, ensuring that the rules are properly applied. Additionally, there are specific edge cases because of the final frame, accounting for previous frames with spares or strikes. These last 3 frames can be reviewed later for further analysis.

```bash
X,X,XXX
X,X,XX1
X,X,X11
X,X,X1/
X,X,1/X
X,X,1/1
X,X,11

X,11,XXX
X,1/,XXX

X,11,XX1
X,11,X11
X,11,X1/
X,11,1/X
X,11,11

X,1/,XX1
X,1/,X11
X,1/,X1/
X,1/,1/X
X,1/,11

1/,X,XXX
1/,X,XX1
1/,X,X11
1/,X,X1/
1/,X,1/X
1/,X,11

1/,11,XXX
1/,11,XX1
1/,11,X11
1/,11,X1/
1/,11,1/X
1/,11,11

1/,1/,XX1
1/,1/,X11
1/,1/,X1/
1/,1/,1/X
1/,1/,11
```