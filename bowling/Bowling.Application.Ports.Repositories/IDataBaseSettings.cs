﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.Ports.Repositories
{
    public interface IDataBaseSettings
    {
        string ConnectionString { get; set; }

        string DatabaseName { get; set; }
    }
}
