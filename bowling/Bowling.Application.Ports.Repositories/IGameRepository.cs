﻿using Bowling.Domain.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.Ports.Repositories
{
    public interface IGameRepository
    {
        Task<Game> FindByIdAsync(string gameId);

        Task<Game> AddAsync(Game game);

        Task<Game> UpdateAsync(Game game);
    }
}
