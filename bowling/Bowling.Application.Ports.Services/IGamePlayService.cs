﻿using Bowling.Domain.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.Ports.Services
{
    public interface IGamePlayService
    {
        Task<string> CreateAsync();
        Task<Game> RecordRollsAsync(string gameId, string[] rolls);
        Task<int> GetScoreAsync(string gameId);
    }
}
