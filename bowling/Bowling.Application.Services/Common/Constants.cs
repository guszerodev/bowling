﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.Services.Common
{
    public class Constants
    {
        public static readonly string STRIKE = "X";
        public static readonly string SPARE = "/";
        public static readonly int MAX_POINTS = 10;
        public static readonly int NUMBER_OF_FRAMES_IN_A_GAME = 10;
    }
}
