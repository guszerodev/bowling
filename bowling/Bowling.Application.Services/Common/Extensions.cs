﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.Services.Common
{
    public static class Extensions
    {
        public static bool Equal(this Rolls roll, string data) => roll switch
        {
            Rolls.Spare => data.ToUpper() == Constants.SPARE,
            Rolls.Strike => data.ToUpper() == Constants.STRIKE,
            _ => false
        };
    }
}
