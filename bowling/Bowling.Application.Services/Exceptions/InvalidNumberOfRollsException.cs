﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.Services.Exceptions
{
    public class InvalidNumberOfRollsException : Exception
    {
        private static readonly string defaultMessage = "Invalid number of Rolls";

        public InvalidNumberOfRollsException()
        {
        }

        public InvalidNumberOfRollsException(string message)
            : base(message)
        {
        }

        public InvalidNumberOfRollsException(string message, Exception inner)
            : base(message, inner)
        {
        }

        public static InvalidNumberOfRollsException Create()
        {
            return new InvalidNumberOfRollsException(defaultMessage);
        }
    }
}
