﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.Services.Exceptions
{
    internal class InvalidRollException : Exception
    {
        private static readonly string defaultMessage = "Unexpected roll was found";
        public string InvalidRoll { get; set; }

        public InvalidRollException()
        {
        }

        public InvalidRollException(string message)
            : base(message)
        {
        }

        public InvalidRollException(string message, Exception inner)
            : base(message, inner)
        {
        }

        public static InvalidRollException Create()
        {
            return new InvalidRollException(defaultMessage);
        }

        public static InvalidRollException Create(string roll)
        {
            var exception = new InvalidRollException(defaultMessage);
            exception.InvalidRoll = roll;
            return exception;
        }
    }
}
