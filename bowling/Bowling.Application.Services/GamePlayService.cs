﻿using Bowling.Application.Ports.Repositories;
using Bowling.Application.Ports.Services;
using Bowling.Application.Services.Exceptions;
using Bowling.Application.Services.GameRules;
using Bowling.Domain.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.Services
{
    public class GamePlayService : IGamePlayService
    {
        private readonly IGameService gameService;

        public GamePlayService(IGameService gameservice)
        {
            this.gameService = gameservice;
        }

        public async Task<string> CreateAsync()
        {
            var game = new Game { Id = Guid.NewGuid().ToString() };
            game = await gameService.AddAsync(game);
            return game.Id;
        }

        public async Task<int> GetScoreAsync(string gameId)
        {
            var game = await gameService.FindByIdAsync(gameId);

            if (game is null)
            {
                throw new GameNotFoundException();
            }
            
            return game.Score;
        }

        public async Task<Game> RecordRollsAsync(string gameId, string[] rolls)
        {
            var game = await gameService.FindByIdAsync(gameId);
            
            if (game is null)
            {
                throw new GameNotFoundException();
            }

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames =  framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            game.Rolls = string.Join(",", rolls);
            game.Score = frames.Sum(frame => frame.Score);

            return await gameService.UpdateAsync(game);
        }
    }
}
