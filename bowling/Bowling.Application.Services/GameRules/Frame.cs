﻿using Bowling.Application.Services.GameRules.Scoring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.Services.GameRules
{
    public class Frame
    {
        public int First { get; set; }
        public int Second { get; set; }
        public int Last { get; set; }
        public int Score { get; set; }
        public IScoringStrategy ScoringStrategy { get; set; }
        public Frame? Next { get; set; }

        public bool IsLast => Next is null;

        public void CalculateScore()
        {
            ScoringStrategy.CalculateScore(this);
        }

        public int GetBonusFromNextFrames()
        {
            return ScoringStrategy.AskBonusFromNextFrames(this);
        }
    }
}
