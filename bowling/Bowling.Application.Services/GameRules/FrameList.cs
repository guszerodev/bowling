﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.Services.GameRules
{
    public class FrameList : IEnumerable<Frame>
    {
        private int count;

        public Frame? First { get; set; }
        public Frame? Last { get; set; }

        public bool IsEmpty => First is null;
        public int Count => count;

        public void Add(Frame frame)
        {
            if (First is null)
            {
                First = frame;
                Last = frame;
            }
            else
            {
                Last.Next = frame;
                Last = Last.Next;
            }

            count++;
        }

        public IEnumerator<Frame> GetEnumerator()
        {
            Frame current = First;
            while (current is not null)
            {
                yield return current;
                current = current.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
