﻿using Bowling.Application.Services.Common;
using Bowling.Application.Services.Exceptions;
using Bowling.Application.Services.GameRules.Scoring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.Services.GameRules
{
    public class FramesBuilder
    {
        private string[] rolls;

        public FramesBuilder(string[] rolls)
        {
            this.rolls = rolls;
        }

        private void ProcessSecondRoll(string roll, Frame frame)
        {
            if (Rolls.Spare.Equal(roll))
            {
                frame.Second = Constants.MAX_POINTS;
                frame.ScoringStrategy = new SpareScoringStrategy();
            }
            else if (int.TryParse(roll, out int value))
            {
                frame.Second = value;
                frame.ScoringStrategy = new DefaultScoringStrategy();
            }
            else
            {
                throw InvalidRollException.Create(roll);
            }
        }

        private void BuildLastFrame(Frame frame, int index)
        {
            bool thereLeftLastTwoRolls = rolls.Length == index + 2;
            if (thereLeftLastTwoRolls)
            {
                ProcessLikeLastTwoNumbers(rolls[index], rolls[index + 1], frame);
                return;
            }

            bool thereLeftLastThreeRolls = rolls.Length == index + 3;
            if (!thereLeftLastThreeRolls)
            {
                throw InvalidNumberOfRollsException.Create();
            }

            string roll1 = rolls[index];
            string roll2 = rolls[index + 1];
            string roll3 = rolls[index + 2];

            bool firstTwoStrikes = ProcessAsStrikes(roll1, roll2, frame);
            if (firstTwoStrikes)
            {
                frame.Last = GetNumberOrStrikeValue(roll3);
                return;
            }

            bool firstTwoStrikeAndNumber = ProcessAsStrikeAndNumber(roll1, roll2, frame);

            if (firstTwoStrikeAndNumber)
            {
                ProcessLastRollAsSpareOrNumber(roll3, frame);
            }
            else
            {
                ProcessRollsAsNumberAndSpare(roll1, roll2, roll3, frame);
            }
        }

        private void ProcessRollsAsNumberAndSpare(string roll1, string roll2, string roll3, Frame frame)
        {
            int value = 0;

            bool isNumberAndSpare = int.TryParse(roll1, out value) && Rolls.Spare.Equal(roll2);

            if (isNumberAndSpare)
            {
                frame.First = value;
                frame.Second = Constants.MAX_POINTS;
                frame.ScoringStrategy = new SpareScoringStrategy();
            }

            if (int.TryParse(roll3, out value))
            {
                frame.Last = value;
            }
            else if (Rolls.Strike.Equal(roll3))
            {
                frame.ScoringStrategy = new SpareStrikeScoringStrategy();
                frame.Last = Constants.MAX_POINTS;
            }
            else
            {
                throw InvalidRollException.Create(roll3);
            }
        }

        private void ProcessLikeLastTwoNumbers(string roll1, string rool2, Frame frame)
        {
            int value2 = 0;
            bool lastRollsAreNumbers =
            int.TryParse(roll1, out int value1) && int.TryParse(rool2, out value2);

            if (lastRollsAreNumbers)
            {
                frame.First = value1;
                frame.Second = value2;
                frame.ScoringStrategy = new DefaultScoringStrategy();
            }
            else
            {
                throw InvalidRollException.Create(roll1);
            }
        }

        private void ProcessLastRollAsSpareOrNumber(string roll, Frame frame)
        {
            if (Rolls.Spare.Equal(roll))
            {
                frame.Last = Constants.MAX_POINTS;
                frame.ScoringStrategy = new StrikeSpareScoringStrategy();
            }
            else if (int.TryParse(roll, out int value))
            {
                frame.Last = value;
                frame.ScoringStrategy = new StrikeScoringStrategy();
            }
            else
            {
                throw InvalidRollException.Create(roll);
            }
        }

        private bool ProcessAsStrikeAndNumber(string roll1, string roll2, Frame frame)
        {
            int value = 0;
            bool isStrikeAndNumber = Rolls.Strike.Equal(roll1) && int.TryParse(roll2, out value);
            if (isStrikeAndNumber)
            {
                frame.First = Constants.MAX_POINTS;
                frame.Second = value;

            }

            return isStrikeAndNumber;
        }

        private int GetNumberOrStrikeValue(string roll)
        {
            if (int.TryParse(roll, out int value))
            {
                return value;
            }
            else if (Rolls.Strike.Equal(roll))
            {
                return Constants.MAX_POINTS;
            }

            throw InvalidRollException.Create(roll);
        }

        private bool ProcessAsStrikes(string roll1, string roll2, Frame frame)
        {
            bool theyAreStrikes = Rolls.Strike.Equal(roll1) && Rolls.Strike.Equal(roll2);
            if (theyAreStrikes)
            {
                frame.First = Constants.MAX_POINTS;
                frame.Second = Constants.MAX_POINTS;
                frame.ScoringStrategy = new StrikeScoringStrategy();
            }

            return theyAreStrikes;
        }

        public FrameList BuildFrames()
        {
            var frames = new FrameList();

            for (int i = 0; i < rolls.Length; i++)
            {
                string roll = rolls[i];
                Frame frame = new Frame();
                frames.Add(frame);

                if (frames.Count == Constants.NUMBER_OF_FRAMES_IN_A_GAME)
                {
                    BuildLastFrame(frame, i);
                    i += 2;
                    continue;
                }

                if (Rolls.Strike.Equal(roll))
                {
                    frame.First = Constants.MAX_POINTS;
                    frame.ScoringStrategy = new StrikeScoringStrategy();
                }
                else if (int.TryParse(roll, out int value))
                {
                    ProcessSecondRoll(rolls[i + 1], frame);
                    frame.First = value;
                    i++;
                }
                else
                {
                    throw InvalidRollException.Create(roll);
                }
            }

            return frames;
        }
    }
}
