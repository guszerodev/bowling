﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.Services.GameRules.Scoring
{
    public class DefaultScoringStrategy : IScoringStrategy
    {
        public int AskBonusFromNextFrames(Frame frame)
        {
            return 0;
        }

        public void CalculateScore(Frame frame)
        {
            frame.Score = frame.First + frame.Second;
        }

        public int GetOneRoll(Frame frame)
        {
            return frame.First;
        }

        public int GetTwoRolls(Frame frame)
        {
            return frame.First + frame.Second;
        }
    }
}
