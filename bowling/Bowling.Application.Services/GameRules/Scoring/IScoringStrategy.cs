﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.Services.GameRules.Scoring
{
    public interface IScoringStrategy
    {
        int AskBonusFromNextFrames(Frame frame);
        void CalculateScore(Frame frame);
        int GetTwoRolls(Frame frame);
        int GetOneRoll(Frame frame);
    }
}
