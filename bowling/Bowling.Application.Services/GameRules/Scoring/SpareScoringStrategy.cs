﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.Services.GameRules.Scoring
{
    public class SpareScoringStrategy : IScoringStrategy
    {
        public int AskBonusFromNextFrames(Frame frame)
        {
            if (frame.IsLast)
            {
                return 0;
            }
            Frame next = frame.Next;
            return next.ScoringStrategy.GetOneRoll(next);
        }

        public void CalculateScore(Frame frame)
        {
            if (frame.IsLast)
            {
                frame.Score = frame.Second + frame.Last;
            }
            else
            {
                frame.Score = frame.Second + frame.GetBonusFromNextFrames();
            }
        }

        public int GetOneRoll(Frame frame)
        {
            return frame.First;
        }

        public int GetTwoRolls(Frame frame)
        {
            return frame.Second;
        }
    }
}
