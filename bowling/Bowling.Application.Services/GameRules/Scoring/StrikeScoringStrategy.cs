﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.Services.GameRules.Scoring
{
    public class StrikeScoringStrategy : IScoringStrategy
    {
        public int AskBonusFromNextFrames(Frame frame)
        {
            if (frame.IsLast)
            {
                return 0;
            }
            Frame next = frame.Next;
            return next.ScoringStrategy.GetTwoRolls(next);
        }

        public void CalculateScore(Frame frame)
        {
            if (frame.IsLast)
            {
                frame.Score = frame.First + frame.Second + frame.Last;
            }
            else
            {
                frame.Score = frame.First + frame.GetBonusFromNextFrames();
            }
        }

        public int GetOneRoll(Frame frame)
        {
            return frame.First;
        }

        public int GetTwoRolls(Frame frame)
        {
            int secondRoll = frame.IsLast ? frame.Second :
                frame.Next.ScoringStrategy.GetOneRoll(frame.Next);

            return frame.First + secondRoll;

        }
    }
}
