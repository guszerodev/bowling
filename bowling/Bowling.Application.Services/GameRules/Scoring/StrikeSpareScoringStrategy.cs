﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.Services.GameRules.Scoring
{
    public class StrikeSpareScoringStrategy : IScoringStrategy
    {
        public int AskBonusFromNextFrames(Frame frame)
        {
            return 0;
        }

        public void CalculateScore(Frame frame)
        {
            if (frame.IsLast)
            {
                frame.Score = frame.First + frame.Last;
            }
        }

        public int GetOneRoll(Frame frame)
        {
            return frame.First;
        }

        public int GetTwoRolls(Frame frame)
        {
            return frame.First + frame.Second;
        }
    }
}
