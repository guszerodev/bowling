﻿using Bowling.Application.Ports.Repositories;
using Bowling.Application.Ports.Services;
using Bowling.Domain.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.Services
{
    public class GameService : IGameService
    {
        private readonly IGameRepository gameRepository;
        public GameService(IGameRepository gameRepository)
        {
            this.gameRepository = gameRepository;
        }

        public async Task<Game> AddAsync(Game game)
        {
            return await gameRepository.AddAsync(game);
        }

        public async Task<Game> FindByIdAsync(string gameId)
        {
            return await gameRepository.FindByIdAsync(gameId);
        }

        public async Task<Game> UpdateAsync(Game game)
        {
            return await gameRepository.UpdateAsync(game);
        }
    }
}
