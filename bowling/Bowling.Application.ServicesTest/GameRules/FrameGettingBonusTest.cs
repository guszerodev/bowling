﻿using Bowling.Application.Services.GameRules.Scoring;
using Bowling.Application.Services.GameRules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.ServicesTest.GameRules
{
    public class FrameGettingBonusTest
    {
        [Fact]
        public void ThreeFramesSpareStrikeDefault_Test()
        {
            // 9/,X,12
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 9,
                Second = 10,
                ScoringStrategy = new SpareScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 2,
                ScoringStrategy = new DefaultScoringStrategy()
            });

            IEnumerator<Frame> enumerator = list.GetEnumerator();
            enumerator.MoveNext();
            Assert.Equal(10, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(3, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
        }

        [Fact]
        public void ThreeFramesStrikeStrikeStrike_Test()
        {
            // X,X,XXX
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 10,
                Last = 10,
                ScoringStrategy = new StrikeScoringStrategy()
            });

            IEnumerator<Frame> enumerator = list.GetEnumerator();
            enumerator.MoveNext();
            Assert.Equal(20, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(20, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
        }

        [Fact]
        public void ThreeFramesStrikeStrikeStrikeMixLastSpare_Test()
        {
            // X,X,X1/
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 1,
                Last = 10,
                ScoringStrategy = new StrikeSpareScoringStrategy()
            });

            IEnumerator<Frame> enumerator = list.GetEnumerator();
            enumerator.MoveNext();
            Assert.Equal(20, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(11, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
        }

        [Fact]
        public void ThreeFramesStrikeStrikeSpareMixLastStrike_Test()
        {
            // X,X,1/X
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                Last = 10,
                ScoringStrategy = new SpareStrikeScoringStrategy()
            });

            IEnumerator<Frame> enumerator = list.GetEnumerator();
            enumerator.MoveNext();
            Assert.Equal(11, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(10, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
        }

        [Fact]
        public void ThreeFramesStrikeStrikeSpareMixLastNumber_Test()
        {
            // X,X,1/1
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                Last = 1,
                // SpareStrike works as well
                ScoringStrategy = new SpareStrikeScoringStrategy()
            });
            IEnumerator<Frame> enumerator = list.GetEnumerator();
            enumerator.MoveNext();
            Assert.Equal(11, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(10, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
        }

        [Fact]
        public void ThreeFramesStrikeStrikeNumbers_Test()
        {
            // X,X,11
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 1,
                ScoringStrategy = new DefaultScoringStrategy()
            });
            IEnumerator<Frame> enumerator = list.GetEnumerator();
            enumerator.MoveNext();
            Assert.Equal(11, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(2, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
        }

        [Fact]
        public void ThreeFramesStrikeNumbersStrike_Test()
        {
            // X,11,XXX
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 1,
                ScoringStrategy = new DefaultScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 10,
                Last = 10,
                ScoringStrategy = new StrikeScoringStrategy()
            });
            IEnumerator<Frame> enumerator = list.GetEnumerator();
            enumerator.MoveNext();
            Assert.Equal(2, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
        }

        [Fact]
        public void ThreeFramesStrikeSpareStrike_Test()
        {
            // X,1/,XXX
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                ScoringStrategy = new SpareScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 10,
                Last = 10,
                ScoringStrategy = new StrikeScoringStrategy()
            });
            IEnumerator<Frame> enumerator = list.GetEnumerator();
            enumerator.MoveNext();
            Assert.Equal(10, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(10, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
        }

        [Fact]
        public void ThreeFramesStrikeNumbersStrikeWithLastNumber_Test()
        {
            // X,11,XX1
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 1,
                ScoringStrategy = new DefaultScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 10,
                Last = 1,
                // Strike works as well
                ScoringStrategy = new StrikeScoringStrategy()
            });
            IEnumerator<Frame> enumerator = list.GetEnumerator();
            enumerator.MoveNext();
            Assert.Equal(2, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
        }

        [Fact]
        public void ThreeFramesStrikeNumbersStrikeWithLastNumbers_Test()
        {
            // X,11,X11
            Frame lastFrame = new Frame
            {
                First = 10,
                Second = 1,
                Last = 1,
                // Strike works as well
                ScoringStrategy = new StrikeScoringStrategy()
            };
            Frame secondFrame = new Frame
            {
                First = 1,
                Second = 1,
                ScoringStrategy = new DefaultScoringStrategy(),
                Next = lastFrame
            };
            Frame firstFrame = new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
                Next = secondFrame
            };

            Assert.Equal(2, firstFrame.GetBonusFromNextFrames());
            Assert.Equal(0, secondFrame.GetBonusFromNextFrames());
            Assert.Equal(0, lastFrame.GetBonusFromNextFrames());
        }

        [Fact]
        public void ThreeFramesStrikeNumbersStrikeLastSpare_Test()
        {
            // X,11,X1/
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 1,
                ScoringStrategy = new DefaultScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 1,
                Last = 10,
                ScoringStrategy = new StrikeSpareScoringStrategy()
            });
            IEnumerator<Frame> enumerator = list.GetEnumerator();
            enumerator.MoveNext();
            Assert.Equal(2, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
        }

        [Fact]
        public void ThreeFramesStrikeNumbersSpareLastStrike_Test()
        {
            // X,11,1/X
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 1,
                ScoringStrategy = new DefaultScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                Last = 10,
                ScoringStrategy = new SpareStrikeScoringStrategy()
            });
            IEnumerator<Frame> enumerator = list.GetEnumerator();
            enumerator.MoveNext();
            Assert.Equal(2, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
        }

        [Fact]
        public void ThreeFramesStrikeNumbersNumbers_Test()
        {
            // X,11,11
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 1,
                ScoringStrategy = new DefaultScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                Last = 10,
                ScoringStrategy = new DefaultScoringStrategy()
            });
            IEnumerator<Frame> enumerator = list.GetEnumerator();
            enumerator.MoveNext();
            Assert.Equal(2, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
        }

        [Fact]
        public void ThreeFramesStrikeSpareStrikeWithLastNumber_Test()
        {
            // X,1/,XX1
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                ScoringStrategy = new SpareScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 10,
                Last = 1,
                ScoringStrategy = new StrikeScoringStrategy()
            });
            IEnumerator<Frame> enumerator = list.GetEnumerator();
            enumerator.MoveNext();
            Assert.Equal(10, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(10, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
        }

        [Fact]
        public void ThreeFramesStrikeSpareStrikeWithLastNumbers_Test()
        {
            // X,1/,X11
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                ScoringStrategy = new SpareScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 1,
                Last = 1,
                // Strike works as well
                ScoringStrategy = new StrikeScoringStrategy()
            });
            IEnumerator<Frame> enumerator = list.GetEnumerator();
            enumerator.MoveNext();
            Assert.Equal(10, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(10, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
        }

        [Fact]
        public void ThreeFramesStrikeSpareStrikeWithLastSpare_Test()
        {
            // X,1/,X1/
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                ScoringStrategy = new SpareScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 1,
                Last = 10,
                ScoringStrategy = new StrikeSpareScoringStrategy()
            });
            IEnumerator<Frame> enumerator = list.GetEnumerator();
            enumerator.MoveNext();
            Assert.Equal(10, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(10, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
        }

        [Fact]
        public void ThreeFramesStrikeSpareSpareWithLastStrike_Test()
        {
            // X,1/,1/X
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                ScoringStrategy = new SpareScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                Last = 10,
                ScoringStrategy = new SpareStrikeScoringStrategy()
            });
            IEnumerator<Frame> enumerator = list.GetEnumerator();
            enumerator.MoveNext();
            Assert.Equal(10, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(1, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
        }

        [Fact]
        public void ThreeFramesSpareStrikeStrikeWithLastSpare_Test()
        {
            // 1/,X,X1/
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                ScoringStrategy = new SpareScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 1,
                Last = 10,
                ScoringStrategy = new StrikeSpareScoringStrategy()
            });
            IEnumerator<Frame> enumerator = list.GetEnumerator();
            enumerator.MoveNext();
            Assert.Equal(10, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(11, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
        }

        [Fact]
        public void ThreeFramesSpareStrikeSpareWithLastStrike_Test()
        {
            // 1/,X,1/X
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                ScoringStrategy = new SpareScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                Last = 10,
                ScoringStrategy = new SpareStrikeScoringStrategy()
            });
            IEnumerator<Frame> enumerator = list.GetEnumerator();
            enumerator.MoveNext();
            Assert.Equal(10, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(10, enumerator.Current.GetBonusFromNextFrames());
            enumerator.MoveNext();
            Assert.Equal(0, enumerator.Current.GetBonusFromNextFrames());
        }
    }
}
