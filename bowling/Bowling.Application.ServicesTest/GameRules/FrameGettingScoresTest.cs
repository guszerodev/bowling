﻿using Bowling.Application.Services.GameRules.Scoring;
using Bowling.Application.Services.GameRules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.ServicesTest.GameRules
{
    public class FrameGettingScoresTest
    {
        [Fact]
        public void ThreeFramesSpareStrikeDefault_Test()
        {
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 9,
                Second = 10,
                ScoringStrategy = new SpareScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 2,
                ScoringStrategy = new DefaultScoringStrategy()
            });

            foreach (var frame in list)
            {
                frame.CalculateScore();
            }

            int[] expected = { 20, 13, 3 };
            int index = 0;
            foreach (var frame in list)
            {
                Assert.Equal(expected[index++], frame.Score);
            }
        }

        [Fact]
        public void ThreeFramesStrikeStrikeStrike_Test()
        {
            // X,X,XXX
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 10,
                Last = 10,
                ScoringStrategy = new StrikeScoringStrategy()
            });
            foreach (var frame in list)
            {
                frame.CalculateScore();
            }

            int[] expected = { 30, 30, 30 };
            int index = 0;
            foreach (var frame in list)
            {
                Assert.Equal(expected[index++], frame.Score);
            }
        }

        [Fact]
        public void ThreeFramesStrikeStrikeStrikeMixLastSpare_Test()
        {
            // X,X,X1/
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 1,
                Last = 10,
                ScoringStrategy = new StrikeSpareScoringStrategy()
            });
            foreach (var frame in list)
            {
                frame.CalculateScore();
            }

            int[] expected = { 30, 21, 20 };
            int index = 0;
            foreach (var frame in list)
            {
                Assert.Equal(expected[index++], frame.Score);
            }
        }

        [Fact]
        public void ThreeFramesStrikeStrikeSpareMixLastStrike_Test()
        {
            // X,X,1/X
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                Last = 10,
                ScoringStrategy = new SpareStrikeScoringStrategy()
            });
            foreach (var frame in list)
            {
                frame.CalculateScore();
            }

            int[] expected = { 21, 20, 20 };
            int index = 0;
            foreach (var frame in list)
            {
                Assert.Equal(expected[index++], frame.Score);
            }
        }

        [Fact]
        public void ThreeFramesStrikeStrikeSpareMixLastNumber_Test()
        {
            // X,X,1/1
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                Last = 1,
                // SpareStrike works as well
                ScoringStrategy = new SpareStrikeScoringStrategy()
            });
            foreach (var frame in list)
            {
                frame.CalculateScore();
            }

            int[] expected = { 21, 20, 11 };
            int index = 0;
            foreach (var frame in list)
            {
                Assert.Equal(expected[index++], frame.Score);
            }
        }

        [Fact]
        public void ThreeFramesStrikeStrikeNumbers_Test()
        {
            // X,X,11
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 1,
                ScoringStrategy = new DefaultScoringStrategy()
            });
            foreach (var frame in list)
            {
                frame.CalculateScore();
            }

            int[] expected = { 21, 12, 2 };
            int index = 0;
            foreach (var frame in list)
            {
                Assert.Equal(expected[index++], frame.Score);
            }
        }

        [Fact]
        public void ThreeFramesStrikeNumbersStrike_Test()
        {
            // X,11,XXX
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 1,
                ScoringStrategy = new DefaultScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 10,
                Last = 10,
                ScoringStrategy = new StrikeScoringStrategy()
            });
            foreach (var frame in list)
            {
                frame.CalculateScore();
            }

            int[] expected = { 12, 2, 30 };
            int index = 0;
            foreach (var frame in list)
            {
                Assert.Equal(expected[index++], frame.Score);
            }
        }

        [Fact]
        public void ThreeFramesStrikeSpareStrike_Test()
        {
            // X,1/,XXX
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                ScoringStrategy = new SpareScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 10,
                Last = 10,
                ScoringStrategy = new StrikeScoringStrategy()
            });
            foreach (var frame in list)
            {
                frame.CalculateScore();
            }

            int[] expected = { 20, 20, 30 };
            int index = 0;
            foreach (var frame in list)
            {
                Assert.Equal(expected[index++], frame.Score);
            }
        }

        [Fact]
        public void ThreeFramesStrikeNumbersStrikeWithLastNumber_Test()
        {
            // X,11,XX1
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 1,
                ScoringStrategy = new DefaultScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 10,
                Last = 1,
                // Strike works as well
                ScoringStrategy = new StrikeScoringStrategy()
            });
            foreach (var frame in list)
            {
                frame.CalculateScore();
            }

            int[] expected = { 12, 2, 21 };
            int index = 0;
            foreach (var frame in list)
            {
                Assert.Equal(expected[index++], frame.Score);
            }
        }

        [Fact]
        public void ThreeFramesStrikeNumbersStrikeWithLastNumbers_Test()
        {
            // X,11,X11
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 1,
                ScoringStrategy = new DefaultScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 1,
                Last = 1,
                // Strike works as well
                ScoringStrategy = new StrikeScoringStrategy()
            });
            foreach (var frame in list)
            {
                frame.CalculateScore();
            }

            int[] expected = { 12, 2, 12 };
            int index = 0;
            foreach (var frame in list)
            {
                Assert.Equal(expected[index++], frame.Score);
            }
        }

        [Fact]
        public void ThreeFramesStrikeNumbersStrikeLastSpare_Test()
        {
            // X,11,X1/
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 1,
                ScoringStrategy = new DefaultScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 1,
                Last = 10,
                ScoringStrategy = new StrikeSpareScoringStrategy()
            });
            foreach (var frame in list)
            {
                frame.CalculateScore();
            }

            int[] expected = { 12, 2, 20 };
            int index = 0;
            foreach (var frame in list)
            {
                Assert.Equal(expected[index++], frame.Score);
            }
        }

        [Fact]
        public void ThreeFramesStrikeNumbersSpareLastStrike_Test()
        {
            // X,11,1/X
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 1,
                ScoringStrategy = new DefaultScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                Last = 10,
                ScoringStrategy = new SpareStrikeScoringStrategy()
            });
            foreach (var frame in list)
            {
                frame.CalculateScore();
            }

            int[] expected = { 12, 2, 20 };
            int index = 0;
            foreach (var frame in list)
            {
                Assert.Equal(expected[index++], frame.Score);
            }
        }

        [Fact]
        public void ThreeFramesStrikeNumbersNumbers_Test()
        {
            // X,11,11
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 1,
                ScoringStrategy = new DefaultScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 1,
                ScoringStrategy = new DefaultScoringStrategy()
            });
            foreach (var frame in list)
            {
                frame.CalculateScore();
            }

            int[] expected = { 12, 2, 2 };
            int index = 0;
            foreach (var frame in list)
            {
                Assert.Equal(expected[index++], frame.Score);
            }
        }

        [Fact]
        public void ThreeFramesStrikeSpareStrikeWithLastNumber_Test()
        {
            // X,1/,XX1
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                ScoringStrategy = new SpareScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 10,
                Last = 1,
                ScoringStrategy = new StrikeScoringStrategy()
            });
            foreach (var frame in list)
            {
                frame.CalculateScore();
            }

            int[] expected = { 20, 20, 21 };
            int index = 0;
            foreach (var frame in list)
            {
                Assert.Equal(expected[index++], frame.Score);
            }
        }

        [Fact]
        public void ThreeFramesStrikeSpareStrikeWithLastNumbers_Test()
        {
            // X,1/,X11
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                ScoringStrategy = new SpareScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 1,
                Last = 1,
                // Strike works as well
                ScoringStrategy = new StrikeScoringStrategy()
            });
            foreach (var frame in list)
            {
                frame.CalculateScore();
            }

            int[] expected = { 20, 20, 12 };
            int index = 0;
            foreach (var frame in list)
            {
                Assert.Equal(expected[index++], frame.Score);
            }
        }

        [Fact]
        public void ThreeFramesStrikeSpareStrikeWithLastSpare_Test()
        {
            // X,1/,X1/
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                ScoringStrategy = new SpareScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 1,
                Last = 10,
                ScoringStrategy = new StrikeSpareScoringStrategy()
            });
            foreach (var frame in list)
            {
                frame.CalculateScore();
            }

            int[] expected = { 20, 20, 20 };
            int index = 0;
            foreach (var frame in list)
            {
                Assert.Equal(expected[index++], frame.Score);
            }
        }

        [Fact]
        public void ThreeFramesStrikeSpareSpareWithLastStrike_Test()
        {
            // X,1/,1/X
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                ScoringStrategy = new SpareScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                Last = 10,
                ScoringStrategy = new SpareStrikeScoringStrategy()
            });
            foreach (var frame in list)
            {
                frame.CalculateScore();
            }

            int[] expected = { 20, 11, 20 };
            int index = 0;
            foreach (var frame in list)
            {
                Assert.Equal(expected[index++], frame.Score);
            }
        }

        [Fact]
        public void ThreeFramesSpareStrikeStrikeWithLastSpare_Test()
        {
            // 1/,X,X1/
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                ScoringStrategy = new SpareScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                Second = 1,
                Last = 10,
                ScoringStrategy = new StrikeSpareScoringStrategy()
            });
            foreach (var frame in list)
            {
                frame.CalculateScore();
            }

            int[] expected = { 20, 21, 20 };
            int index = 0;
            foreach (var frame in list)
            {
                Assert.Equal(expected[index++], frame.Score);
            }
        }

        [Fact]
        public void ThreeFramesSpareStrikeSpareWithLastStrike_Test()
        {
            // 1/,X,1/X
            FrameList list = new FrameList();
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                ScoringStrategy = new SpareScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 10,
                ScoringStrategy = new StrikeScoringStrategy(),
            });
            list.Add(new Frame
            {
                First = 1,
                Second = 10,
                Last = 10,
                ScoringStrategy = new SpareStrikeScoringStrategy()
            });
            foreach (var frame in list)
            {
                frame.CalculateScore();
            }

            int[] expected = { 20, 20, 20 };
            int index = 0;
            foreach (var frame in list)
            {
                Assert.Equal(expected[index++], frame.Score);
            }
        }
    }
}
