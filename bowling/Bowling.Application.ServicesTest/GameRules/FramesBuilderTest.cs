﻿using Bowling.Application.Services.GameRules.Scoring;
using Bowling.Application.Services.GameRules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.ServicesTest.GameRules
{
    public class FramesBuilderTest
    {
        [Fact]
        public void BasicFull_Test()
        {
            string[] rolls = new string[] { "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X" };

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames = framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            Assert.Equal(10, frames.Count);

            foreach (var frame in frames)
            {
                Assert.Equal(typeof(StrikeScoringStrategy), frame.ScoringStrategy.GetType());
            }

            Assert.Equal(300, frames.Sum(frame => frame.Score));
        }

        [Fact]
        public void AlmostFullFailLastOne_Test()
        {
            string[] rolls = new string[] { "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "9" };

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames = framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            Assert.Equal(10, frames.Count);

            foreach (var frame in frames)
            {
                Assert.Equal(typeof(StrikeScoringStrategy), frame.ScoringStrategy.GetType());
            }

            Assert.Equal(299, frames.Sum(frame => frame.Score));
        }

        [Fact]
        public void AlmostFullFailLastTwo_Test()
        {
            string[] rolls = new string[] { "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "8", "1" };

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames = framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            Assert.Equal(10, frames.Count);

            foreach (var frame in frames)
            {
                //if (frame.IsLast)
                //{
                //    continue;
                //}
                Assert.Equal(typeof(StrikeScoringStrategy), frame.ScoringStrategy.GetType());
            }

            Assert.Equal(287, frames.Sum(frame => frame.Score));
        }

        [Fact]
        public void AlmostFullFailLastTwoWithSpare_Test()
        {
            string[] rolls = new string[] { "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "9", "/" };

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames = framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            Assert.Equal(10, frames.Count);

            foreach (var frame in frames)
            {
                if (frame.IsLast)
                {
                    Assert.Equal(typeof(StrikeSpareScoringStrategy), frame.ScoringStrategy.GetType());
                    continue;
                }
                Assert.Equal(typeof(StrikeScoringStrategy), frame.ScoringStrategy.GetType());
            }

            Assert.Equal(289, frames.Sum(frame => frame.Score));
        }

        [Fact]
        public void AlmostFullFailLastTwoWithSpareFromZero_Test()
        {
            string[] rolls = new string[] { "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "0", "/" };

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames = framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            Assert.Equal(10, frames.Count);

            foreach (var frame in frames)
            {
                if (frame.IsLast)
                {
                    Assert.Equal(typeof(StrikeSpareScoringStrategy), frame.ScoringStrategy.GetType());
                    continue;
                }
                Assert.Equal(typeof(StrikeScoringStrategy), frame.ScoringStrategy.GetType());
            }

            Assert.Equal(280, frames.Sum(frame => frame.Score));
        }

        [Fact]
        public void AlmostFullFailLastFrameWithSpareAndFinalStrike_Test()
        {
            string[] rolls = new string[] { "X", "X", "X", "X", "X", "X", "X", "X", "X", "1", "/", "X" };

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames = framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            Assert.Equal(10, frames.Count);

            foreach (var frame in frames)
            {
                if (frame.IsLast)
                {
                    Assert.Equal(typeof(SpareStrikeScoringStrategy), frame.ScoringStrategy.GetType());
                    continue;
                }
                Assert.Equal(typeof(StrikeScoringStrategy), frame.ScoringStrategy.GetType());
            }

            Assert.Equal(271, frames.Sum(frame => frame.Score));
        }

        [Fact]
        public void AlmostFullFailNiveFrame_Test()
        {
            string[] rolls = new string[] { "X", "X", "X", "X", "X", "X", "X", "X", "1", "1", "X", "X", "X" };

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames = framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            Assert.Equal(10, frames.Count);

            foreach (var frame in frames)
            {
                if (!frame.IsLast && frame.Next.IsLast)
                {
                    Assert.Equal(typeof(DefaultScoringStrategy), frame.ScoringStrategy.GetType());
                    continue;
                }
                Assert.Equal(typeof(StrikeScoringStrategy), frame.ScoringStrategy.GetType());
            }

            Assert.Equal(245, frames.Sum(frame => frame.Score));
        }

        [Fact]
        public void AlmostFullFailNineFrameZeroSpare_Test()
        {
            string[] rolls = new string[] { "X", "X", "X", "X", "X", "X", "X", "X", "0", "/", "X", "X", "X" };

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames = framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            Assert.Equal(10, frames.Count);

            foreach (var frame in frames)
            {
                if (!frame.IsLast && frame.Next.IsLast)
                {
                    Assert.Equal(typeof(SpareScoringStrategy), frame.ScoringStrategy.GetType());
                    continue;
                }
                Assert.Equal(typeof(StrikeScoringStrategy), frame.ScoringStrategy.GetType());
            }

            Assert.Equal(270, frames.Sum(frame => frame.Score));
        }

        [Fact]
        public void AlmostFullFailNineAndTenFramesWithOnes_Test()
        {
            string[] rolls = new string[] { "X", "X", "X", "X", "X", "X", "X", "X", "1", "1", "1", "1" };

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames = framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            Assert.Equal(10, frames.Count);

            foreach (var frame in frames)
            {
                if (!frame.IsLast && frame.Next.IsLast)
                {
                    Assert.Equal(typeof(DefaultScoringStrategy), frame.ScoringStrategy.GetType());
                    continue;
                }

                if (frame.IsLast)
                {
                    Assert.Equal(typeof(DefaultScoringStrategy), frame.ScoringStrategy.GetType());
                    continue;
                }
                Assert.Equal(typeof(StrikeScoringStrategy), frame.ScoringStrategy.GetType());
            }

            Assert.Equal(217, frames.Sum(frame => frame.Score));
        }

        [Fact]
        public void AlmostFullFailNineAndTenFramesNiveWithSpare_Test()
        {
            string[] rolls = new string[] { "X", "X", "X", "X", "X", "X", "X", "X", "1", "/", "1", "1" };

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames = framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            Assert.Equal(10, frames.Count);

            foreach (var frame in frames)
            {
                if (!frame.IsLast && frame.Next.IsLast)
                {
                    Assert.Equal(typeof(SpareScoringStrategy), frame.ScoringStrategy.GetType());
                    continue;
                }

                if (frame.IsLast)
                {
                    Assert.Equal(typeof(DefaultScoringStrategy), frame.ScoringStrategy.GetType());
                    continue;
                }
                Assert.Equal(typeof(StrikeScoringStrategy), frame.ScoringStrategy.GetType());
            }

            Assert.Equal(234, frames.Sum(frame => frame.Score));
        }

        [Fact]
        public void AlmostFullFailNineAndTenFramesNiveWithSpareTenWithSpareAndStrike_Test()
        {
            string[] rolls = new string[] { "X", "X", "X", "X", "X", "X", "X", "X", "0", "/", "0", "/", "X" };

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames = framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            Assert.Equal(10, frames.Count);

            foreach (var frame in frames)
            {
                if (!frame.IsLast && frame.Next.IsLast)
                {
                    Assert.Equal(typeof(SpareScoringStrategy), frame.ScoringStrategy.GetType());
                    continue;
                }

                if (frame.IsLast)
                {
                    Assert.Equal(typeof(SpareStrikeScoringStrategy), frame.ScoringStrategy.GetType());
                    continue;
                }
                Assert.Equal(typeof(StrikeScoringStrategy), frame.ScoringStrategy.GetType());
            }

            Assert.Equal(250, frames.Sum(frame => frame.Score));
        }

        [Fact]
        public void AlmostFullFailEightFrameNiveStrikeTenWithStrikeAndZeros_Test()
        {
            string[] rolls = new string[] { "X", "X", "X", "X", "X", "X", "X", "0", "0", "X", "X", "0", "0" };

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames = framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            Assert.Equal(10, frames.Count);

            Assert.Equal(typeof(StrikeScoringStrategy), frames.Last.ScoringStrategy.GetType());

            Assert.Equal(210, frames.Sum(frame => frame.Score));
        }

        [Fact]
        public void AlmostFullFailEightFrameNiveStrikeTenWithSpare_Test()
        {
            string[] rolls = new string[] { "X", "X", "X", "X", "X", "X", "X", "5", "4", "X", "9", "/", "0" };

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames = framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            Assert.Equal(10, frames.Count);

            Assert.Equal(typeof(SpareScoringStrategy), frames.Last.ScoringStrategy.GetType());

            Assert.Equal(233, frames.Sum(frame => frame.Score));
        }

        [Fact]
        public void AlmostFullFailSevenFrameEightSpareNiveSpareTenWithStrike_Test()
        {
            string[] rolls = new string[] {
                "X", "X", "X", "X", "X", "X", "5", "/", "5", "/", "2", "/", "X", "1", "0" };

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames = framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            Assert.Equal(10, frames.Count);

            Assert.Equal(typeof(StrikeScoringStrategy), frames.Last.ScoringStrategy.GetType());

            Assert.Equal(223, frames.Sum(frame => frame.Score));
        }

        [Fact]
        public void AlmostFullFailSevenFrameEightStrikeNiveStrikeTenStrikeWithZeros_Test()
        {
            string[] rolls = new string[] {
                "X", "X", "X", "X", "X", "X", "3", "/", "X", "X", "X", "0", "0" };

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames = framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            Assert.Equal(10, frames.Count);

            Assert.Equal(typeof(StrikeScoringStrategy), frames.Last.ScoringStrategy.GetType());

            Assert.Equal(243, frames.Sum(frame => frame.Score));
        }

        [Fact]
        public void WithoutStrikesOrSpares_Test()
        {
            string[] rolls = new string[] {
                "0", "1", "2", "3", "4", "5", "6", "3", "7", "2", "8", "1", "9", "0", "0", "1", "2", "3", "4", "5" };

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames = framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            Assert.Equal(10, frames.Count);

            foreach (var frame in frames)
            {
                Assert.Equal(typeof(DefaultScoringStrategy), frame.ScoringStrategy.GetType());
            }

            Assert.Equal(66, frames.Sum(frame => frame.Score));
        }

        [Fact]
        public void WithoutStrikesJustOneSpare_Test()
        {
            string[] rolls = new string[] {
                "1", "2", "3", "4", "5", "4", "6", "/", "7", "2", "8", "1", "9", "0", "0", "1", "2", "3", "4", "5" };

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames = framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            Assert.Equal(10, frames.Count);

            Assert.Equal(typeof(DefaultScoringStrategy), frames.Last.ScoringStrategy.GetType());

            Assert.Equal(78, frames.Sum(frame => frame.Score));
        }

        [Fact]
        public void WithoutSpareJustOneStrike_Test()
        {
            string[] rolls = new string[] {
                "0", "1", "2", "3", "4", "5", "X", "9", "0", "1", "2", "3", "4", "5", "4", "0", "1", "2", "3" };

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames = framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            Assert.Equal(10, frames.Count);

            Assert.Equal(typeof(DefaultScoringStrategy), frames.Last.ScoringStrategy.GetType());

            Assert.Equal(68, frames.Sum(frame => frame.Score));
        }

        [Fact]
        public void OneStrikeAfterSpare_Test()
        {
            string[] rolls = new string[] {
                "0", "1", "2", "3", "4", "5", "6", "/", "X", "0", "1", "2", "3", "4", "5", "6", "3", "0", "1" };

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames = framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            Assert.Equal(10, frames.Count);

            Assert.Equal(typeof(DefaultScoringStrategy), frames.Last.ScoringStrategy.GetType());

            Assert.Equal(71, frames.Sum(frame => frame.Score));
        }

        [Fact]
        public void OneSpareAfterStrike_Test()
        {
            string[] rolls = new string[] {
                "0", "1", "2", "3", "4", "5", "X", "6", "/", "1", "2", "3", "4", "5", "4", "0", "1", "2", "3" };

            var framesBuilder = new FramesBuilder(rolls);
            FrameList frames = framesBuilder.BuildFrames();
            foreach (var frame in frames)
            {
                frame.CalculateScore();
            }

            Assert.Equal(10, frames.Count);

            Assert.Equal(typeof(DefaultScoringStrategy), frames.Last.ScoringStrategy.GetType());

            Assert.Equal(71, frames.Sum(frame => frame.Score));
        }
    }
}
