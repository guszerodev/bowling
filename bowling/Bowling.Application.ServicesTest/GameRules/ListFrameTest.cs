﻿using Bowling.Application.Services.GameRules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.ServicesTest.GameRules
{
    public class ListFrameTest
    {
        [Fact]
        public void EmptyList_Test()
        {
            FrameList list = new FrameList();
            Assert.True(list.IsEmpty);
        }

        [Fact]
        public void OneFrameList_Test()
        {
            FrameList list = new FrameList();
            list.Add(new Frame());
            Assert.False(list.IsEmpty);
            Assert.Equal(list.First, list.Last);
        }

        [Fact]
        public void FrameListEnumerationLoop_Test()
        {
            FrameList list = new FrameList();
            list.Add(new Frame { First = 1 });
            list.Add(new Frame { First = 2 });
            list.Add(new Frame { First = 3 });
            Assert.False(list.IsEmpty);
            Assert.NotEqual(list.First, list.Last);
            Assert.Equal(3, list.Count);
            int count = 1;
            foreach (var frame in list)
            {
                Assert.Equal(count++, frame.First);
            }
        }

        [Fact]
        public void FrameListEnumerator_Test()
        {
            FrameList list = new FrameList();
            list.Add(new Frame { First = 1 });
            list.Add(new Frame { First = 2 });
            list.Add(new Frame { First = 3 });
            Assert.False(list.IsEmpty);
            Assert.NotEqual(list.First, list.Last);
            Assert.Equal(3, list.Count);

            IEnumerator<Frame> enumerator = list.GetEnumerator();
            Assert.True(enumerator.MoveNext());
            Assert.Equal(1, enumerator.Current.First);
            Assert.True(enumerator.MoveNext());
            Assert.Equal(2, enumerator.Current.First);
            Assert.True(enumerator.MoveNext());
            Assert.Equal(3, enumerator.Current.First);
        }
    }
}
