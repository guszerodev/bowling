﻿using Bowling.Application.Ports.Repositories;
using Bowling.Application.Services;
using Bowling.Domain.Model.Entities;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.ServicesTest
{
    public class GameServiceTest
    {
        [Fact]
        public async Task AddAsync_TestAsync()
        {
            IGameRepository gameRepository = A.Fake<IGameRepository>();
            var gameService = new GameService(gameRepository);
            Game game = A.Fake<Game>();
            A.CallTo(() => gameRepository.AddAsync(game)).Returns(game);
            Game addeGame = await gameService.AddAsync(game);

            Assert.Equal(game, addeGame);
        }
    }
}
