﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bowling.Domain.Model.Entities
{
    public class Game
    {
        public string Id { get; set; }
        public string? Rolls { get; set; }
        public int Score { get; set; }
    }
}
