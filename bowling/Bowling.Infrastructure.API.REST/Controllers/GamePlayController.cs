﻿using Bowling.Application.Ports.Services;
using Bowling.Domain.Model.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;

namespace Bowling.Infrastructure.API.REST.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [ApiVersion("1.0")]
    [ResponseCache(VaryByHeader = "User-Agent", Duration = 5)]
    public class GamePlayController : ControllerBase
    {
        private readonly ILogger<GamePlayController> logger;
        private readonly IGamePlayService gamePlayService;

        public GamePlayController(
            ILogger<GamePlayController> logger,
            IGamePlayService gamePlayService)
        {
            this.logger = logger;
            this.gamePlayService = gamePlayService;
        }

        [HttpPost]
        public async Task<ActionResult<string>> Post()
        {
            string gameId = await gamePlayService.CreateAsync();

            return Created(string.Empty, gameId);
        }

        [HttpGet]
        public async Task<ActionResult<int>> Get(string gameId)
        {
            var score = await gamePlayService.GetScoreAsync(gameId);

            return Ok(score);
        }

        [HttpPut]
        public async Task<ActionResult<Game>> Update(string gameId, [FromBody] string[] rolls)
        {
            var game = await gamePlayService.RecordRollsAsync(gameId, rolls);

            return Ok(game);
        }
    }
}
