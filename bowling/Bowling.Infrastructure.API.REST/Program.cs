using Bowling.Application.Ports.Repositories;
using Bowling.Application.Ports.Services;
using Bowling.Application.Services;
using Bowling.Infrastructure.Repositories.Mongo;
using Bowling.Infrastructure.Repositories.SQLEntityFramework.Context;
using Bowling.Infrastructure.Repositories.SQLEntityFramework.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace Bowling.Infrastructure.API.REST
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddApiVersioning(config =>
            {
                config.DefaultApiVersion = new ApiVersion(1, 0);
                config.AssumeDefaultVersionWhenUnspecified = true;
                config.ReportApiVersions = true;
            });

            builder.Services.AddControllers();
            builder.Services.AddResponseCaching();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddScoped<IGameService, GameService>();
            builder.Services.AddScoped<IGamePlayService, GamePlayService>();

            //builder.Services.AddDbContext<BowlingContext>(options =>
            //    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"))
            //    .LogTo(Console.WriteLine, LogLevel.Information)
            //    .EnableSensitiveDataLogging());

            //builder.Services.AddTransient<IGameRepository, GameRepository>();

            builder.Services.Configure<DataBaseSettings>(builder.Configuration.GetSection("MongoDB"));
            builder.Services.AddScoped<IDataBaseSettings>(options => options.GetRequiredService<IOptions<DataBaseSettings>>().Value);
            builder.Services.AddTransient<IGameRepository, GameMongoRepository>();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }
            else
            {
                //app.UseExceptionHandler("/error");
            }

            app.UseHttpsRedirection();
            app.UseResponseCaching();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}