﻿using Bowling.Application.Ports.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Infrastructure.Repositories.Mongo
{
    public class DataBaseSettings : IDataBaseSettings
    {
        public string? ConnectionString { get; set; }

        public string? DatabaseName { get; set; }
    }
}
