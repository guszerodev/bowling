﻿using Bowling.Application.Ports.Repositories;
using MongoDB.Driver;
using MongoModels = Bowling.Infrastructure.Repositories.Mongo.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Formats.Asn1.AsnWriter;
using Bowling.Domain.Model.Entities;
using Microsoft.VisualBasic;
using MongoDB.Bson;

namespace Bowling.Infrastructure.Repositories.Mongo
{
    public class GameMongoRepository : MongoRepository<MongoModels.Game>, IGameRepository
    {
        private const string NAME = "games";

        public GameMongoRepository(IDataBaseSettings dataBaseSettings)
            : base(NAME, dataBaseSettings)
        {

        }

        public async Task<Game> AddAsync(Game game)
        {
            var mongoGame = new MongoModels.Game
            {
                Rolls = game.Rolls,
                Score = game.Score,
            };
            await Collection.InsertOneAsync(mongoGame);
            game.Id = mongoGame.Id.ToString();
            
            return game;
        }

        public async Task<Game> FindByIdAsync(string gameId)
        {
            var mongoId = ObjectId.Parse(gameId);
            var games = await Collection.FindAsync(game => game.Id == mongoId);
            var mongoGame = await games.FirstOrDefaultAsync();

            var game = new Game
            {
                Id = mongoGame.Id.ToString(),
                Rolls = mongoGame.Rolls,
                Score = mongoGame.Score,
            };
            
            return game;
        }

        public async Task<Game> UpdateAsync(Game game)
        {
            var mongoGame = new MongoModels.Game
            {
                Id = ObjectId.Parse(game.Id),
                Rolls = game.Rolls,
                Score = game.Score,
            };

            await Collection.ReplaceOneAsync(game => game.Id  == mongoGame.Id, mongoGame);

            return game;
        }
    }
}
