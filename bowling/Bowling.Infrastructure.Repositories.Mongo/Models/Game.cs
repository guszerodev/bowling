﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Infrastructure.Repositories.Mongo.Models
{
    public class Game
    {
        public ObjectId Id { get; set; }
        public string? Rolls { get; set; }
        public int Score { get; set; }
    }
}
