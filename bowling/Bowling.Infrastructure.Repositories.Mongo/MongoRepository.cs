﻿using Bowling.Application.Ports.Repositories;
using MongoDB.Driver;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Formats.Asn1.AsnWriter;

namespace Bowling.Infrastructure.Repositories.Mongo
{
    public class MongoRepository<TEntity>
    {
        public string CollectionName { get; }
        protected IMongoDatabase Store { get; }
        protected IMongoCollection<TEntity> Collection { get; }

        public MongoRepository(string collectionName, IDataBaseSettings dataBaseSettings)
        {
            CollectionName = collectionName;
            Store = new MongoClient(dataBaseSettings.ConnectionString).GetDatabase(dataBaseSettings.DatabaseName);
            Collection = Store.GetCollection<TEntity>(CollectionName);
        }
    }
}
