﻿using Bowling.Domain.Model.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Infrastructure.Repositories.SQLEntityFramework.Context
{
    public class BowlingContext : DbContext
    {
        public DbSet<Game> Games { get; set; }

        public BowlingContext(DbContextOptions<BowlingContext> options) : base(options)
        {
            //Database.EnsureDeleted();
            //Database.EnsureCreated();
        }
    }
}
