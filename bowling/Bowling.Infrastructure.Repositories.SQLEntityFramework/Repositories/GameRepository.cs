﻿using Bowling.Application.Ports.Repositories;
using Bowling.Domain.Model.Entities;
using Bowling.Infrastructure.Repositories.SQLEntityFramework.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Infrastructure.Repositories.SQLEntityFramework.Repositories
{
    public class GameRepository : IGameRepository
    {
        protected readonly BowlingContext context;
        public GameRepository(BowlingContext context)
        {
            this.context = context;
        }

        public async Task<Game> AddAsync(Game game)
        {
            await context.Games.AddAsync(game);
            await context.SaveChangesAsync();

            return game;
        }

        public async Task<Game> FindByIdAsync(string gameId)
        {
            return await context
                .Games
                .Where(game => game.Id == gameId)
                .FirstOrDefaultAsync();
        }

        public async Task<Game> UpdateAsync(Game game)
        {
            context.Games.Update(game);
            await context.SaveChangesAsync();

            return game;
        }
    }
}
